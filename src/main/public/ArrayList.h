#pragma once
#include <vector>

/**
	My implimintation of Java's ArrayList and ArrayList iterator

	@author Jacob M. Hughes
	@date 3/24/2019
*/
template <class Object>
class ArrayList
{
private:
	/**
		Doubly linked list which stores the data.
	*/
	struct Node {
		Node* next = nullptr;
		Node* prev = nullptr;
		Object object;
		Node() {}
		Node(Object input) { object = input; }
	};
	Node* head = nullptr;
	Node* tail = nullptr;
	int size = 0;

	/**
		Used to remove a node from the linked list and update surrounding pointers
		@param node to be removed
		@return pointer to the previous node, or the new head node if head was removed
	*/
	Node* removeNode(Node* node) {
		Node* next, *previous;
		size--;
		if (node == head) {
			next = node->next;
			delete node;
			head = next;
			head->prev = nullptr;
			return head;
		}
		else if (node == tail) {
			previous = node->prev;
			delete node;
			tail = previous;
			tail->next = nullptr;
			return tail;
		}
		else {
			next = node->next;
			previous = node->prev;
			delete node;
			previous->next = next;
			next->prev = previous;
			return previous;
		}
	}
protected:
public:

	/**
		Default constructor, creates empty list
	*/
	ArrayList() { head = nullptr; }

	/**
		Copy cunstructor, default copy constructor deletes original objects
	*/
	ArrayList(const ArrayList& al) {
		Node* current = al.head;
		while (current) {
			this->add(current->object);
			current = current->next;
		}
	}

	/**
		Destructor, calls clear to delete all pointer's objects
	*/
	~ArrayList() { clear(); }

	/**
		Iterator class for the ArrayList, Makes it easy to loop through the list
	*/
	class Iterator {
	public:

		/**
			Iterator constructor
			@param beginning node, the node before the first object
			@param refrence to the array it is linked to
		*/
		Iterator(Node* headNode,ArrayList& list) : parent(list) {
			node = headNode;
			beginVal = headNode;
		}

		~Iterator() { 
			delete beginVal;
			beginVal = nullptr;
		}

		/**
			Removes the currently accessed object
		*/
		void remove() {
			node = parent.removeNode(node);
		}

		/**
			Resents the iterator to the node before the first object
		*/
		void begin() {
			node = beginVal;
		}

		/**
			Checks if the object has another object after it.
			@return bool, true if another object exists
		*/
		bool hasNext() {
			if (node->next) {
				return true;
			}
			return false;
		}

		/**
			Checks if the object has another object before it.
			@return bool, true if another object exists
		*/
		bool hasPrevious() {
			if (node->prev) {
				return true;
			}
			return false;
		}
		
		/**
			Increases the stack by one and returns the object
			@return refrence to the object
		*/
		Object& next() {
			node = node->next;
			return node->object;
		}

		/**
			Decreases the stack by one and returns the object
			@return refrence to the object
		*/
		Object& previous() {
			node = node->prev;
			return node->object;
		}
	private:
		Node* node = nullptr;
		Node* beginVal = nullptr;
		ArrayList& parent;
	};

	/**
		Creates the iterator object for the current ArrayList
		@return ArrayList<Object>::Iterator
	*/
	Iterator iterator() {
		Node* temp = new Node;
		temp->next = head;
		return Iterator(temp,*this);
	}

	/**
		Appends the specified element to the end of this list.
		@param Object to be added
	*/
	void add(Object object) {
		if (tail == nullptr) {
			head = new Node(object);
			tail = head;
		}
		else {
			tail->next = new Node(object);
			tail->next->prev = tail;
			tail = tail->next;
		}
		size++;
	}

	/**
		Appends all of the elements in the vector to the end of this list.
		@param vector of Objects
	*/
	void addAll(std::vector<Object> objects) {
		for (auto it = objects.begin(); it != objects.end(); it++) {
			add(*it);
		}
	}

	/**
		Appends all of the elements in the ArrayList to the end of this list.
		@param ArrayList of objects to be added
	*/
	void addAll(ArrayList<Object> objects) {
		for (int i = 0; i < objects.getSize(); i++) {
			this->add(objects.get(i));
		}
	}

	/**
		Removes all of the elements from this list.
	*/
	void clear() {
		Node* next;
		Node* current = head;
		while (current != nullptr) {
			next = current->next;
			delete current;
			current = next;
		}
		head = nullptr;
		tail = nullptr;
		size = 0;
	}


	/**
		Returns true if this list contains the specified element.
		@param Object for comparison
		@return boolean
	*/
	bool contains(Object object) {
		Node* temp = head;
		while (temp) {
			if (temp->object == object) {
				return true;
			}
			temp = temp->next;
		}
		return false;
	}

	/**
		Returns the element at the specified position in this list.
		@param position of wanted item
		@return object at given position
	*/
	Object get(int index) {
		if (index > size) {
			return NULL;
		}
		else {
			Node* temp;
			if (index <= (size - index -1)) {
				//start from head
				temp = head;
				int current = 0;
				while (temp) {
					if (current == index) {
						return temp->object;
					}
					current++;
					temp = temp->next;
				}
			}
			else {
				//start from tail
				temp = tail;
				int current = size -1;
				while (temp) {
					if (current == index) {
						return temp->object;
					}
					current--;
					temp = temp->prev;
				}
			}
		}
	}
	
	/**
		Returns the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
		@param object to find
		@return postion of object
	*/
	int indexOf(Object object) {
		Node* temp = head;
		int index = 0;
		while (temp) {
			if (temp->object == object) {
				return index;
			}
			temp = temp->next;
			index++;
		}
		return -1;
	}

	/**
		Returns true if this list contains no elements.
		@return bool
	*/
	bool isEmpty() {
		if (head) {
			return false;
		}
		return true;
	}

	/**
		Returns the index of the last occurrence of the specified element in this list, or -1 if this list does not contain the element.
		@param object to find
		@return index of last occurence of the object
	*/
	int lastIndexOf(Object object) {
		Node* temp = head;
		int index = 0;
		int found = -1;
		while (temp) {
			if (temp->object == object) {
				found = index;
			}
			temp = temp->next;
			index++;
		}
		return found;
	}

	/**
		Removes the element at the specified position in this list.
		@param index to remove, index starts at 0 -> size - 1
		@return true on success
	*/
	bool removeIndex(int index) {
		if (index > size) {
			return false;
		}
		else {
			Node* temp;
			if (index <= (size - index-1)) {
				//start from head
				temp = head;
				int current = 0;
				while (temp) {
					if (current == index) {
						removeNode(temp);
						return true;
					}
					current++;
					temp = temp->next;
				}
			}
			else {
				//start from tail
				temp = tail;
				int current = size-1;
				while (temp) {
					if (current == index) {
						removeNode(temp);
						return true;
					}
					current--;
					temp = temp->prev;
				}
			}
		}
	}

	/**
		Removes the first occurrence of the specified element from this list, if it is present.
		@param object to be removed from list
		@return true on success
	*/
	bool removeObject(Object object) {
		Node* temp = head;
		while (temp) {
			if (temp->object == object) {
				removeNode(temp);
				return true;
			}
			temp = temp->next;
		}
		return false;
	}

	/**
		Removes all of the occurences of the element.
		@param object to be removed from list
		@return number of objects removed
	*/
	int removeAllObject(Object object) {
		int numRemoved = 0;
		Node* temp = head;
		while (temp) {
			if (temp->object == object) {
				temp = removeNode(temp);
				numRemoved++;
			}
			temp = temp->next;
		}
		return numRemoved;
	}

	/**
		Removes from this list all of the elements whose index is between start, inclusive, and end, exclusive.
		@param starting index to be removed
		@param ending index to be removed; exclusive
		@return true on success
	*/
	bool removeRange(int start, int end) {
		if (start > size || end > size) {
			return false;
		}
		else {
			Node* temp;
			if (start <= (size - start -1)) {
				//start from head
				temp = head;
				int current = 0;
				while (temp) {
					if (current>=start && current<end) {
						temp = removeNode(temp);
					}
					current++;
					temp = temp->next;
				}
			}
			else {
				//start from tail
				temp = tail;
				int current = size-1;
				while (temp) {
					if (current >= start && current < end) {
						temp = removeNode(temp);
					}
					current--;
					temp = temp->prev;
				}
			}
		}
		return true;
	}

	/**
		Replaces the element at the specified position in this list with the specified element.
		@param index of object to be replaced, index range; 0->size - 1
		@param object to be swapped in its place
		@return true on success
	*/
	bool set(int index, Object object) {
		if (index > size) {
			return false;
		}
		else {
			Node* temp;
			if (index <= (size - index)) {
				//start from head
				temp = head;
				int current = 0;
				while (temp) {
					if (current == index) {
						temp->object = object;
						return true;
					}
					current++;
					temp = temp->next;
				}
			}
			else {
				//start from tail
				temp = tail;
				int current = size -1;
				while (temp) {
					if (current == index) {
						temp->object = object;
						return true;
					}
					current--;
					temp = temp->prev;
				}
			}
		}
	}

	/**
		Returns the number of elements in this list.
		@return int
	*/
	int getSize() {
		return size;
	}

	/**
	 * 	Overloads backet operator to enable access like an array
	 * 	@param int index
	 * 	@return Object reference
	*/
	Object& operator [](int index){
		Node* temp;
		if (index <= (size - index)) {
			//start from head
			temp = head;
			int current = 0;
			while (temp) {
				if (current == index) {
					return temp->object;
				}
				current++;
				temp = temp->next;
			}
		}
		else {
			//start from tail
			temp = tail;
			int current = size -1;
			while (temp) {
				if (current == index) {
					return temp->object;
				}
				current--;
				temp = temp->prev;
			}
		}
	}
};


