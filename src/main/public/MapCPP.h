#pragma once
#include "ArrayList.h"
#include <map>

template <class Key,class Value>
class MapCPP {
private:
    std::map<Key,Value> object;
    typename std::map<Key,Value>::iterator it;

    std::pair<Key,Value> pair(Key k, Value v){
        return std::pair<Key,Value>(k,v);
    }
protected:
public:
	/**
		Default constructor, creates empty list
	*/
	MapCPP() {}

	/**
		Destructor, calls clear to delete all pointer's objects
	*/
	~MapCPP() { clear(); }

    /**
     * Removes all elements from the map
     */
    void clear(){
        object.clear();
    }

    /**
     * Returns true if the key exists in the map
     * @param key to be checked for
     * @return bool
     */
    bool containsKey(Key key){
        it = object.find(key);
        if(it != object.end()){ 
            return true;
        }
        return false;
    }

    /**
     * Returns true if the value exists in the map
     * @param value to be checked for
     * @return bool
     */
    bool containsValue(Value value){
        it = object.begin();
        while(it != object.end()){
            if( it->second == value){
                return true;
            }
            it++;
        }
        return false;
    }

    /**
     * Returns the value associated with the given key
     * @param key to find
     * @return value
     */
    Value get(Key key){
        it = object.find(key);
        if(it != object.end()){
            return it->second;
        }
        return Value();
    }

    /**
     * Returns true if map is empty
     * @return bool
     */
    bool isEmpty(){
        if(object.size() >0){
            return false;
        }
        return true;
    }

    /**
     * Returns an arraylist of all the keys in the map
     * @return ArrayList<Key>
     */
    ArrayList<Key> keySet(){
        return ArrayList<Key>();
    }

    /**
     * Inserts the key value pair into the map
     * @param key to be inserted
     * @param value associated with key
     * @returns bool
     */
    void put(Key key, Value value){
        object.insert(std::pair<Key,Value>(key,value));
    }

    /**
     * Adds all values from given map to MapCPP
     * @param std::map<Key,Value>
     */
    void putAll(std::map<Key,Value> map){
        it = map.begin();
        while( it != map.end){
            object.insert(pair(it->first,it->second));
            it++;
        }
    }
    
    /**
     * Adds all values from given map to MapCPP
     * @param MapCPP<Key, Value>
     * 
     */
    void putAll(MapCPP<Key,Value> map){

    }

    /**
     * Add Key value if key does not exist
     * @param Key
     * @param Value
     */
    void putIfAbsent(Key key, Value value){
        it = object.begin();
        while ( it != object.end()){
            if(it->first == key){
                return;
            }
            it++;
        }
        object.insert(pair(key,value));
    }

    /**
     * Removes key and value from the map
     * @param key to be removed
     * @return bool
     */
    bool remove(Key key){
        it = object.find(key);
        if(it != object.end()){
            object.erase(it);
            return true;
        }
        return false;
    }

    /**
     * Removes key and value from the map
     * @param key to be removed
     * @param value of key
     * @return bool
     */
    bool remove(Key key, Value value){
        it = object.find(key);
        if(it != object.end()){
            if( it->second == value){
                object.erase(it);
                return true;
            }
        }
        return false;
    }

    /**
     * Replaces the value of the given key
     * @param key
     * @param value
     * @return true on success
     */
    bool replace(Key key, Value value){
        it = object.find(key);
        if(it != object.end()){
            it->second = value;
            return true;
        }
        return false;
    }

    /**
     * Replaces the value of the given key with the given new value
     * @param key
     * @param value to be replace
     * @param new value to insert
     * @return true on success
     */
    bool replace(Key key, Value oldValue, Value newValue){
        it = object.find(key);
        if(it != object.end()){
            if( it->second == oldValue){
                it->second = newValue;
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the size of the Map
     * @return int
     */
    int size(){
        return object.size();
    }
};