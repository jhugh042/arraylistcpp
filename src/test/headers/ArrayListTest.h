#pragma once
#include "ArrayList.h"

#include <cassert>
#include <iostream>

namespace ArrayList_Test{
	void AddTest() {
		std::cout << "[test] Checking add function..." << std::flush;
		ArrayList<int> object;
		for (int i = 1; i <= 10; i++) {
			object.add(i);
		}
		assert(object.getSize() == 10);
		assert(object.get(0) == 1);
		assert(object.get(1) == 2);
		std::cout << " pass" << std::endl;
	}
	void AddAllTest() {
		std::cout << "[test] Checking addAll(vector) function..." << std::flush;
		//vector
		ArrayList<int> object;
		std::vector<int> myVals;
		for (int i = 0; i <= 10; i++) {
			myVals.push_back(i);
		}
		object.addAll(myVals);
		assert(object.contains(4));
		assert(!object.contains(11));
		assert(object.getSize() == 11);
		std::cout << " pass" << std::endl;
	}
	void AddAllTest2() {
		std::cout << "[test] Checking addAll(ArrayList) function..." << std::flush;
		//ArrayList
		ArrayList<int> object;
		ArrayList<int> object2;
		for (int i = 0; i <= 10; i++) {
			object2.add(i);
		}
		object.addAll(object2);
		assert(object.contains(4));
		assert(!object.contains(11));
		std::cout << " pass" << std::endl;
	}
	void ClearTest() {
		std::cout << "[test] Checking clear function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		assert(!object.isEmpty());
		object.clear();
		assert(object.isEmpty());
		std::cout << " pass" << std::endl;
	}
	void ContainsTest() {
		std::cout << "[test] Checking contains function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		assert(object.contains(4));
		assert(!object.contains(11));
		std::cout << " pass" << std::endl;
	}
	void getTest() {
		std::cout << "[test] Checking get function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		assert(5 == object.get(5));
		std::cout << " pass" << std::endl;
	}
	void IndexOfTest() {
		std::cout << "[test] Checking indexOf function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		assert(5 == object.indexOf(5));
		std::cout << " pass" << std::endl;
	}
	void IsEmptyTest() {
		std::cout << "[test] Checking isEmtpy function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		assert(!object.isEmpty());
		object.clear();
		assert(object.isEmpty());
		std::cout << " pass" << std::endl;
	}
	void LastIndexOfTest() {
		std::cout << "[test] Checking lastIndexOf function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		object.add(5);
		assert(object.getSize()-1 == object.lastIndexOf(5));
		std::cout << " pass" << std::endl;
	}
	void RemoveIndexTest() {
		std::cout << "[test] Checking removeIndex function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		object.removeIndex(5);
		assert(object.getSize() == 10);
		assert(!object.contains(5));
		assert(object.get(5) == 6);
		object.clear();
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		object.removeIndex(10);
		assert(object.getSize() == 10);
		assert(!object.contains(10));
		std::cout << " pass" << std::endl;
	}
	void RemoveObjectTest() {
		std::cout << "[test] Checking removeObject function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		object.removeObject(5);
		assert(object.getSize() == 10);
		assert(!object.contains(5));
		assert(6 == object.get(5));
		object.clear();
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		object.removeObject(0);
		assert(object.getSize() == 10);
		assert(!object.contains(0));
		assert(1 == object.get(0));
		std::cout << " pass" << std::endl;
	}
	void RemoveAllObjectTest() {
		std::cout << "[test] Checking removeAllObject function..." << std::flush;
		ArrayList<int> object;
		for (int i = 1; i <= 10; i++) {
			object.add(i);
		}
		object.add(5);
		object.removeAllObject(5);
		assert(object.getSize() == 9);
		assert(!object.contains(5));
		assert(7 == object.get(5));
		std::cout << " pass" << std::endl;
	}
	void RemoveRangeTest() {
		std::cout << "[test] Checking removeRange function..." << std::flush;
		ArrayList<int> object;
		for (int i = 1; i <= 10; i++) {
			object.add(i);
		}
		object.removeRange(2, 6);
		assert(!object.contains(3));
		assert(object.contains(7));
		assert(object.getSize() == 6);
		std::cout << " pass" << std::endl;
	}
	void SetTest() {
		std::cout << "[test] Checking set function..." << std::flush;
		ArrayList<int> object;
		for (int i = 1; i <= 10; i++) {
			object.add(i);
		}
		object.set(0, 100);
		assert(object.get(0) == 100);
		object.set(3, 500);
		assert(object.get(3) == 500);
		assert(object.contains(500));
		std::cout << " pass" << std::endl;
	}
	void GetSizeTest() {
		std::cout << "[test] Checking getSize function..." << std::flush;
		ArrayList<int> object;
		for (int i = 1; i <= 10; i++) {
			object.add(i);
		}
		assert(object.getSize() == 10);
		std::cout << " pass" << std::endl;
	}
	void IteratorTest() {
		std::cout << "[test] Checking Iterator class..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		ArrayList<int>::Iterator it = object.iterator();
		while (it.hasNext()) {
			int val = it.next();
			assert(0 <= val);
			assert(10 >= val);
		}
		std::cout << " pass" << std::endl;
	}
	void ArrayOperatorTest() {
		std::cout << "[test] Checking Array Operator function..." << std::flush;
		ArrayList<int> object;
		for (int i = 0; i <= 10; i++) {
			object.add(i);
		}
		assert(object[0] == 0);
		object[0] = 20;
		assert(object[0] == 20);
		std::cout << " pass" << std::endl;
	}

	void ArrayList_TestAll(){
		AddTest();
		AddAllTest();
		AddAllTest2();
		ClearTest();
		ContainsTest();
		getTest();
		IndexOfTest();
		IsEmptyTest();
		LastIndexOfTest();
		RemoveIndexTest();
		RemoveObjectTest();
		RemoveAllObjectTest();
		RemoveRangeTest();
		SetTest();
		GetSizeTest();
		IteratorTest();
		ArrayOperatorTest();
	}
}