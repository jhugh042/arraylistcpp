#pragma once
#include "MapCPP.h"

#include <cassert>
#include <iostream>

namespace MapCPP_Test{

    void ClearTest(){
        std::cout << "[test] Checking clear function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(object.size() == 10);
        object.clear();
        assert(object.size() == 0);
        std::cout << " pass" << std::endl;
    }

    void ContainsKeyTest(){
        std::cout << "[test] Checking containsKey function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(object.containsKey(1));
        assert(!object.containsKey(11));
        std::cout << " pass" << std::endl;
    }

    void ContainsValueTest(){
        std::cout << "[test] Checking containsValue function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(object.containsValue(true));
        assert(!object.containsValue(false));
        std::cout << " pass" << std::endl;
    }

    void GetTest(){
        std::cout << "[test] Checking get function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(object.get(1) == true);
        std::cout << " pass" << std::endl;
    }

    void IsEmptyTest(){
        std::cout << "[test] Checking isEmpty function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(!object.isEmpty());
        object.clear();
        assert(object.isEmpty());
        std::cout << " pass" << std::endl;
    }

    void KeySetTest(){
        std::cout << "[test] Checking keySet function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        ArrayList<int> arry = object.keySet();
        assert(arry.contains(9));
        std::cout << " pass" << std::endl;
    }

    void PutTest(){
        std::cout << "[test] Checking put function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(object.size() == 10);
        assert(object.get(1) == true);
        std::cout << " pass" << std::endl;
    }

    void PutAllTest_map(){
        //todo
    }

    void PutAllTest_MapCPP(){
        std::cout << "[test] Checking putAll(MapCPP) function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        MapCPP<int,bool> object2;
        object2.putAll(object);
        assert(object2.containsKey(3));
        std::cout << " pass" << std::endl;
    }

    void PutIfAbsentTest(){
        std::cout << "[test] Checking putIfAbsent function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        object.putIfAbsent(5,false);
        assert(object.get(5) == true);
        object.putIfAbsent(11,false);
        assert(object.get(11) == false);
        std::cout << " pass" << std::endl;
    }

    void RemoveTest_K(){
        std::cout << "[test] Checking remove(key) function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        object.remove(5);
        assert(!object.containsKey(5));
        std::cout << " pass" << std::endl;
    }

    void RemoveTest_KV(){
        std::cout << "[test] Checking remove(key,value) function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        object.remove(5,true);
        assert(!object.containsKey(5));
        std::cout << " pass" << std::endl;
    }

    void ReplaceTest_KV(){
        std::cout << "[test] Checking replace(key,value) function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        object.replace(5,false);
        assert(object.get(5) == false);
        std::cout << " pass" << std::endl;
    }

    void ReplaceTest_KVV(){
        std::cout << "[test] Checking replace(key,value,value) function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        object.replace(5,true,false);
        assert(object.get(5) == false);
        std::cout << " pass" << std::endl;
    }

    void SizeTest(){
        std::cout << "[test] Checking size function..." << std::flush;
        MapCPP<int,bool> object;
        for (int i = 1; i <= 10; i++) {
            object.put(i,true);
        }
        assert(object.size() == 10);
        object.clear();
        assert(object.size() == 0);
        std::cout << " pass" << std::endl;
    }

    void MapCPP_TestAll(){
        ClearTest();
        ContainsKeyTest();
        ContainsValueTest();
        GetTest();
        IsEmptyTest();
        //KeySetTest();
        PutTest();
        PutAllTest_map();
        //PutAllTest_MapCPP();
        PutIfAbsentTest();
        RemoveTest_K();
        RemoveTest_KV();
        ReplaceTest_KV();
        ReplaceTest_KVV();
        SizeTest();
    }
}