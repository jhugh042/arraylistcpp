#include "ArrayListTest.h"
#include "MapCPPTest.h"

#include <string>

void printTitle(std::string title){
    int wantedLen = 60;
    int tLen = title.length();
    if(tLen %2 != 0){
        title += ' ';
    }
    int printNum = (wantedLen - tLen)/2;
     std::cout << std::string(printNum, '=') << title 
                << std::string(printNum, '=') << std::endl;
}

int main(int argc, char** argv) {
    printTitle("ArrayList");
    ArrayList_Test::ArrayList_TestAll();
    printTitle("End ArrayList");
    printTitle("MapCPP");
    MapCPP_Test::MapCPP_TestAll();
    printTitle("End MapCPP");
	return 0;
}