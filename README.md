# ArrayListCPP

For the purpose of the demos, I will be using a class `Object` but you can replace this with whatever datatype you like.

### Install

ArrayListCPP is entirely contained within the ArrayList.h header file (src/main/public/). To install just download the header file
and put it wherever you like then just include it in your project.

## Creating an ArrayList
An empty ArrayList can be created by writing `ArrayList<Object> myArrayList`
```cpp
    ArrayList<Object> myArrayList;
```
## Adding data to ArrayList
Data can be added to the ArrayList in two ways.
### Add
`add(Object)` is used for adding a single object to the end of the list.
```cpp
    myArrayList.add(Object());
```
### Add All
`addAll(std::vector<Object>)` is used for adding all vector items to the end of the list.
```cpp
    std::vector<Object> myvector;
    myArrayList.addAll(myvector);
```
## Clearing the ArrayList
The list can be cleared of all of its data by either leaving the scope or calling `clear()`.
```cpp
    myArrayList.cler();
```
## Contents of the ArrayList
### Contains
`contains(Object)` returns a bool, true if object exists in the list;
```cpp
    if(myArrayList.contains(Object()){
        //do stuff
    }
```
### Get
`get(int)` returns the object at the given index.
```cpp
    Object value = myArrayList.get(0);
```
### Index OF
`indexOf(Object)` returns the location of the first instance of the given object.
```cpp
    int value = myArrayList.get(Object());
```
### Is Empty
`isEmpty()` returns a bool, true if the list is empty.
```cpp
    if(myArrayList.isEmpty()){
        //do stuff
    }
```
### Last Index Of
`lastIndexOf(Object)` will return the position of the last given object.
```cpp
    int value = myArrayList.lastIndexOf(Object());
```
### Remove Index
`removeIndex(int)` will remove the object at the position.
```cpp
    myArrayList.removeIndex(0);
```
### Remove Object
`removeObject(Object)` will remove the first matching object.
```cpp
    myArrayList.removeObject(Object());
```
### Remove All Objects
`removeAllObject(Object)` will remove all matching objects.
```cpp
    myArrayList.removeAllObject(Object());
```
### Remove Range
`removeRange(int start,int end)` will remove all objects in the range, end is exclusive
```cpp
    myArrayList.removeRange(0,2);
    //removes objects at positions 0 and 1
```
### Set
`set(int,Object)` will replace the object at the position with the given object.
```cpp
    myArrayList.set(0,Object());
```
### Get Size
`getSize()` returns the size of the ArrayList.
```cpp
    int arraySize = myArrayList.getSize();
```

# Using the Iterator
The iterator was built to work much like how Java's Iterator does.
## Creating the iterator object
When creating the iterator object the first object it points to is NULL
```cpp
    ArrayList<Object>::Iterator it = myArrayList.iterator();
    //or
    auto it = myArrayList.iterator();
```
## Looping through the Iterator Object
The next function returns a reference to the object, this means you can grab either the value of the object or you can update the item itself
```cpp
    while(it.hasNext()){
        //for making changes to the objects value
        Object& value = it.next();

        //for keeping a persistent value
        Object value = it.next();
    }
```
## Reverse parse the Iterator
The iterator can be sent back to the beginning by useing `it.begin()`, this sets it to the object just before the first object.
`it.hasPrevious()` and `it.previous()` can be used to go back one element.
```cpp
    it.begin();
    it.next();
    it.next();
    if(it.hasPrevious()){
        it.previous();
    }
```

# Demo
```cpp
    //create the arraylist
    ArrayList<int> myArrayList;

    //add numbers 0-10 (inclusive)
    for(int i=0; i <= 10; i++){
        myArrayList.add(i);
    }
    //removes the int at position 0 which is 0
    myArrayList.removeIndex(0);
    //add object 5 to the end of the list
    myArrayList.add(5);
    //removes the first object 5, the added one is still there
    myArrayList.removeObject(5);
    //create iterator object
    ArrayList<int>::Iterator it = myArrayList.iterator();
    //loop through all elements
    while(it.hasNext()){
        //get refrence to object
        int& value = it.next();
        //add one to object
        value++;
    }

    //reset iterator to beginning
    it.begin();
    while(it.hasNext()){
        std::cout<<it.next()<<", ";
        //prints out:
        //2, 3, 4, 5, 7, 8, 9, 10, 11, 6,
    }
```